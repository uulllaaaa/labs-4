﻿using System;



namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(PierwszyKomponent.Class1);
        public static Type Component2 = typeof(DrugiKomponent.Class2);

        public static Type RequiredInterface = typeof(Lab4.Contract.Interface1);

        public static GetInstance GetInstanceOfRequiredInterface = (component2) => new DrugiKomponent.Class2();
        
        #endregion

        #region P2

        public static Type Container = typeof(ComponentFramework.Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { };

        public static AreDependenciesResolved ResolvedDependencied = (container) => false;

        #endregion

        #region P3

        public static Type Component2B = typeof(DrugiKomponentB.Class2b);

        #endregion
    }
}
